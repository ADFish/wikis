# README #

This repository contains the wikis to explain the structure of following project. The source code and related paper of this project are saved in Private Repository. Please contact me (amyduanyurou@gmail.com) if you want to know more.

## Sensor-based multi-modal worker performance prediction at workplace
Work performance is important for human resource management and productivity, as wearable sensors provide a way to collect human behavior data consciously and automatically, the aim of this work is to establish a novel way to apply wearable sensor to predict numeric work performance in the workplace instead of the traditional questionnaire which is subjective and time-consuming.

In sensor-based numeric performance prediction works, though communication behavior is proved to be important, the influence of it keeps unexplored. Prediction performance could be improved due to the lack of exploiting domain knowledge on the way of communication in business organization, such as communication with respect to the job role and place characteristics. Therefore, we investigate multiple communication modalities related to worker performance then squeeze a new feature descriptor based on communication between workers from wireless signal strength and infrared signals.

Furthermore, as multiple KPIs (Key Performance Indicators) such as the sales volume and customer satisfaction are considered simultaneously in real life, pervious performance prediction method which can predict only one kind of KPI is not practical. To make the prediction from sensors practical, we also build a multitask regression approach to enhance the prediction accuracy by making use of the relationships between different tasks.


## センサーに基づく個人間のコミュニケーション機能とマルチタスク回帰による職場でのマルチモーダル労働者パフォーマンス予測
仕事のパフォーマンスは、人的資源の管理と生産性にとって重要です。ウェアラブルセンサーは、人間の行動データを意識的かつ自動的に収集する方法を提供するため、この作業の目的は、主観的で時間がかかる従来のアンケートの代わりに、ウェアラブルセンサーを適用して職場の数値作業パフォーマンスを予測する新しい方法を確立することです。

センサーベースの数値パフォーマンス予測では、コミュニケーションが重要であることが証明されていますが、その影響は未踏のままです。仕事の役割や場所の特性に関するコミュニケーションなど、ビジネス組織のコミュニケーション方法に関するドメインの知識を活用できないため、予測のパフォーマンスが向上する可能性があります。したがって、作業者のパフォーマンスに関連する複数の通信様式を調査し、無線信号強度と赤外線信号からの作業者間の通信に基づいて新しい機能記述子を絞り込みます。

さらに、販売量や顧客満足度などの複数のKPI（主要業績評価指標）が実生活で同時に考慮されるため、1種類のKPIのみを予測できる従来のパフォーマンス予測方法は実用的ではありません。センサーからの予測を実用的にするために、異なるタスク間の関係を利用して予測精度を高めるマルチタスク回帰アプローチも構築します。
